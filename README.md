# Introduction
This repository contains several UVW-coordinate related tools.

To use the tools, make sure that Python, Matplotlib, and Numpy are available. On the DAS-6 cluster, you can use `source scripts/load-modules.sh` to do so.

# Parsing LOFAR and SKA coordinates
The `data` directory contains coordinates for LOFAR (`lofar` subdirectory) and for the SKA (`ska` subdirectory).

Both the subdirectories contain scripts to plot the coordinates:
```
./data/lofar/parse-lofar.py <layout>
```
With `<layout>` either `HBA` or `LBA`.

```
./data/ska/parse-ska.py <file>
```
With `<file>` one of the filenames in `data/ska`, e.g. `SKA1_low_ecef.txt` or `SKA1_mid_ecef.txt`.

# Coordinates prepared for IDG
The `data/parsed` directories contains several data files that are prepared to be used with IDG. There is another script to plot this data:
```
./data/parsed/plot-uvw.py <file>
```
With `<file>` for instance `LOFAR_hba.txt`, or `SKA1_low.txt`.

# UVW simulator
The `uvwsim` subdirectory contains an example of how the [uvwsim](https://github.com/SKA-ScienceDataProcessor/uvwsim/) library is used to generate realistic UVW data based on the antenna or station coordinates.
To run this:
```
cd uvwsim
./run.sh
```

# IDG example
The `idg` subdirectory provides C++ code to demonstrate how UVW coordinates are generated in IDG.
To run this:
```
cd idg
make
./main.x
```