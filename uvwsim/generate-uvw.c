#include <iostream>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>
#include <string.h>

#include "uvwsim.h"

/*
    Macro
*/
#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

/*
    Constants
*/
#define RANDOM_SEED         1234
#define SPEED_OF_LIGHT      299792458.0

/*
    Observation parameters
*/
#define START_FREQUENCY     500.0e6
#define FREQUENCY_INCREMENT 5.0e6
#define RIGHT_ASCENSION     10.0 * (M_PI/180.)
#define DECLINATION         70.0 * (M_PI/180.)
#define YEAR                2015
#define MONTH               03
#define DAY                 20
#define HOUR                01
#define MINUTE              57
#define SECONDS             1.3
#define OBSERVATION_HOURS   10

void* generate_uvw(int nr_stations, int nr_time, int nr_channels, int w_planes, int gridsize, int subgridsize) {
    // Check whether layout file exists
    const char* filename = getenv("LAYOUT_FILE");
    if (!filename) {
        std::cerr << "LAYOUT_FILE environment variable not specified.";
        exit(EXIT_FAILURE);
    }
    if (!uvwsim_file_exists(filename)) {
        std::cerr << "Unable to find specified layout file: " << filename << std::endl;
        exit(EXIT_FAILURE);
    }

    // Read the number of stations in the layout file
    int nr_stations_file = uvwsim_get_num_stations(filename);

    // Check wheter the requested number of station is feasible
    if (nr_stations_file < nr_stations) {
        std::cerr << "More stations requested than present in layout file: "
                  << "(" << nr_stations_file << ")" << std::endl;
    }

    // Allocate memory for antenna coordinates
    double *x = (double*) malloc(nr_stations_file * sizeof(double));
    double *y = (double*) malloc(nr_stations_file * sizeof(double));
    double *z = (double*) malloc(nr_stations_file * sizeof(double));

    // Load the antenna coordinates
    if (uvwsim_load_station_coords(filename, nr_stations_file, x, y, z) != nr_stations_file) {
        std::cerr << "Failed to read antenna coordinates." << std::endl;
        exit(EXIT_FAILURE);
    }

    // Select some antennas randomly when not all antennas are requested
    if (nr_stations < nr_stations_file) {
        // Allocate memory for selection of antenna coordinates
        double *_x = (double*) malloc(nr_stations * sizeof(double));
        double *_y = (double*) malloc(nr_stations * sizeof(double));
        double *_z = (double*) malloc(nr_stations * sizeof(double));

        // Generate nr_stations random numbers
        int station_number[nr_stations];
        int i = 0;
        srandom(RANDOM_SEED);
        while (i < nr_stations) {
            int index = nr_stations * ((double) random() / RAND_MAX);
            bool found = true;
            for (int j = 0; j < i; j++) {
                if (station_number[j] == index) {
                    found = false;
                    break;
                }
            }
            if (found) {
                station_number[i++] = index;
            }
        }

        // Set stations
        for (int i = 0; i < nr_stations; i++) {
            _x[i] = x[station_number[i]];
            _y[i] = y[station_number[i]];
            _z[i] = z[station_number[i]];
        }

        // Swap pointers and free memory
        double *__x = x;
        double *__y = y;
        double *__z = z;
        x = _x;
        y = _y;
        z = _z;
        free(__x);
        free(__y);
        free(__z);
    }

    // Define observation parameters
    double ra0  = RIGHT_ASCENSION;
    double dec0 = DECLINATION;
    double start_time_mjd = uvwsim_datetime_to_mjd(YEAR, MONTH, DAY, HOUR, MINUTE, SECONDS);
    double obs_length_days = OBSERVATION_HOURS / 24.0;
    int nr_baselines = uvwsim_num_baselines(nr_stations);

    // Allocate memory for baseline coordinates
    int nr_coordinates = nr_time * nr_baselines;
    double *uu = (double*) malloc(nr_coordinates * sizeof(double));
    double *vv = (double*) malloc(nr_coordinates * sizeof(double));
    double *ww = (double*) malloc(nr_coordinates * sizeof(double));

    // Evaluate baseline uvw coordinates
    for (int t = 0; t < nr_time; t++) {
        double time_mjd = start_time_mjd + t * (obs_length_days/(double)nr_time);
        size_t offset = t * nr_baselines;
        uvwsim_evaluate_baseline_uvw(
            &uu[offset], &vv[offset], &ww[offset],
            nr_stations, x, y, z, ra0, dec0, time_mjd);
    }

    // Allocate memory for uvw datastructure
    typedef struct { float u, v, w; } UVW;
    typedef UVW UVWType[nr_baselines][nr_time][nr_channels];
    UVWType *uvw = (UVWType *) malloc(sizeof(UVWType));

    // Convert baseline coordinates from metres to wavelengths
    for (int c = 0, j = 0; c < nr_channels; ++c) {
        double freq = START_FREQUENCY + (double) c * FREQUENCY_INCREMENT;
        float freq_scale = freq / SPEED_OF_LIGHT;
        for (int i = 0; i < nr_coordinates; ++i, ++j) {
            int bl = i % nr_baselines;
            int t = i / nr_baselines;
            int index = bl * nr_time * nr_channels + t * nr_channels + c;
            UVW value = {float(uu[i]) * freq_scale,
                         float(vv[i]) * freq_scale,
                         float(ww[i]) * freq_scale};
            (*uvw)[bl][t][c] = value;
        }
    }

    // Find minimum and maxmium u, v and w values
    UVW min = {0, 0, 0};
    UVW max = {0, 0, 0};
    for (int bl = 0; bl < nr_baselines; bl++) {
        for (int t = 0; t < nr_time; t++) {
            for (int c = 0; c < nr_channels; c++) {
                UVW value = (*uvw)[bl][t][c];
                if (value.u < min.u) min.u = value.u;
                if (value.v < min.v) min.v = value.v;
                if (value.w < min.w) min.w = value.w;
                if (value.u > max.u) max.u = value.u;
                if (value.v > max.v) max.v = value.v;
                if (value.w > max.w) max.w = value.w;
            }
        }
    }

    // Scale uvw to to grid and collapse into w-planes
    float scale_u = ((gridsize/2.0f)-subgridsize-1) / MAX(abs(min.u), max.u);
    float scale_v = ((gridsize/2.0f)-subgridsize-1) / MAX(abs(min.v), max.v);
    float scale_w = (w_planes/2) / MAX(abs(min.w), max.w);
    for (int bl = 0; bl < nr_baselines; bl++) {
        for (int t = 0; t < nr_time; t++) {
            for (int c = 0; c < nr_channels; c++) {
                UVW value = (*uvw)[bl][t][c];
                value.u = scale_u * value.u + (gridsize/2.0f);
                value.v = scale_v * value.v + (gridsize/2.0f);
                value.w = scale_w * value.w + (w_planes/2.0f);
                (*uvw)[bl][t][c] = value;
            }
        }
    }

    // Free memory
    free(x); free(y); free(z);
    free(uu); free(vv); free(ww);

    // Return scaled uvw coordinates
    return (void *) uvw;
}

int main(int argc, char **argv) {
    int nr_stations = 10;
    int nr_baselines = (nr_stations * (nr_stations - 1)) / 2;
    int nr_time = 512;
    int nr_channels = 3;
    int w_planes = 10;
    int gridsize = 4096;
    int subgridsize = 32;

    typedef struct { float u, v, w; } UVW;
    typedef UVW UVWType[nr_baselines][nr_time][nr_channels];
    UVWType *uvw = (UVWType *) generate_uvw(nr_stations, nr_time, nr_channels, w_planes, gridsize, subgridsize);

    for (int bl = 0; bl < nr_baselines; bl++) {
        for (int t = 0; t < nr_time; t++) {
            for (int c = 0; c < nr_channels; c++) {
                UVW value = (*uvw)[bl][t][c];
                printf("%f,%f,%f\n", value.u, value.v, value.w);
            }
        }
    }
}
