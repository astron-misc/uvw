#!/bin/env python3

# imports
import os
import sys
import numpy as np
import matplotlib.pyplot as plt

# read command line arguments
argv = sys.argv
argc = len(argv)
if argc != 2:
    print(("Usage:\n\t%s file" %(argv[0])))
    exit()
filename = argv[1]

# read data
script_dir=os.path.dirname(os.path.realpath(__file__))
data=open(f"{script_dir}/{filename}").read().split()

# split data in u, v and w
interval = 1
antenna_u=np.asarray([float(e.split(",")[0]) for e in data[::interval]])
antenna_v=np.asarray([float(e.split(",")[1]) for e in data[::interval]])
antenna_w=np.asarray([float(e.split(",")[2]) for e in data[::interval]])
interval = 8
station_u=np.asarray([float(e.split(",")[0]) for e in data[::interval]])
station_v=np.asarray([float(e.split(",")[1]) for e in data[::interval]])
station_w=np.asarray([float(e.split(",")[2]) for e in data[::interval]])

# Print station coordinates
for i in range(len(station_u)):
    print("%.3f,%.3f,%.3f" % (station_u[i], station_v[i], station_w[i]))

# open figure
fig = plt.figure()
ax = fig.add_subplot(1,1,1)

# add grid to figure
ax.grid(which='both')

# make figure square
ax.set_aspect(1)

# plot data
plt.plot(antenna_u, antenna_v, "ko", ms=1)
plt.plot(station_u, station_v, "ro", ms=3)

# show figure
plt.show()
