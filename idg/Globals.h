#pragma once

#include <complex>

/* Parameters */
const int NR_TIMESTEPS = 32;
const int NR_POLARIZATIONS = 4;
const int NR_STATIONS = 5;
const int NR_BASELINES = (NR_STATIONS * (NR_STATIONS-1)) / 2;
const int INTEGRATION_TIME = 1;

/* Structures */
typedef struct { float u, v, w; } UVW;

/* Datatype */
typedef UVW UVWType[NR_BASELINES][NR_TIMESTEPS];
