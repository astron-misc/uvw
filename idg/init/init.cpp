#include <iostream>
#include <cstdint>
#include <cstring>
#include <cmath>
#include <limits>

#include "init.h"


static const char* LAYOUT_DIR = "./init";
static const char* LAYOUT_FILE = "SKA1_low_ecef.txt";
static const int RANDOM_SEED = 1236;
static const double RIGHT_ASCENSION = (10.0 * (M_PI/180.));
static const double DECLINATION = (70.0 * (M_PI/180.));
static const int YEAR = 2014;
static const int MONTH = 03;
static const int DAY = 20;
static const int HOUR = 01;
static const int MINUTE = 57;
static const double SECONDS = 1.3;
static const int DEFAULT_INTEGRATION_TIME = 1;


void print_uvw(void *ptr, int nr_baselines, int nr_time) {
    UVWType *uvw = (UVWType *) ptr;

    for (int bl = 0; bl < nr_baselines; bl++) {
        printf("baseline: %d\n", bl);
        for (int t = 0; t < nr_time; t++) {
            int i = t * nr_baselines + bl;
            UVW value = (*uvw)[bl][t];
            float u = value.u;
            float v = value.v;
            float w = value.w;
            printf("u = %f, v = %f, w = %f\n", u, v, w);
        }
        printf("\n");
    }
}


void init_uvw(void *ptr, int nr_stations, int nr_baselines,
              int nr_time, int integration_time = DEFAULT_INTEGRATION_TIME)
{
    typedef struct { float u, v, w; } UVW;
    typedef UVW UVWType[nr_baselines][nr_time];

    UVWType *uvw = (UVWType *) ptr;

    // Check whether layout file exists
    bool found = false;
    char filename[512];
    sprintf(filename, "./%s/%s", LAYOUT_DIR, LAYOUT_FILE);

    // Read the number of stations in the layout file.
    int nr_stations_file = uvwsim_get_num_stations(filename);

    // Allocate memory for antenna coordinates
    double *x = (double*) malloc(nr_stations_file * sizeof(double));
    double *y = (double*) malloc(nr_stations_file * sizeof(double));
    double *z = (double*) malloc(nr_stations_file * sizeof(double));

    // Load the antenna coordinates
    #if defined(DEBUG)
    printf("looking for stations file in: %s\n", filename);
    #endif

    if (uvwsim_load_station_coords(filename, nr_stations_file, x, y, z) != nr_stations_file) {
        std::cerr << "Failed to read antenna coordinates." << std::endl;
        exit(EXIT_FAILURE);
    }

    // Select some antennas randomly when not all antennas are requested
    if (nr_stations < nr_stations_file) {
        // Allocate memory for selection of antenna coordinates
        double *_x = (double*) malloc(nr_stations * sizeof(double));
        double *_y = (double*) malloc(nr_stations * sizeof(double));
        double *_z = (double*) malloc(nr_stations * sizeof(double));

        // Generate nr_stations random numbers
        int station_number[nr_stations];
        int i = 0;
        srandom(RANDOM_SEED);
        while (i < nr_stations) {
            int index = nr_stations_file * ((double) random() / RAND_MAX);
            bool found = true;
            for (int j = 0; j < i; j++) {
                if (station_number[j] == index) {
                    found = false;
                    break;
                }
            }
            if (found) {
                station_number[i++] = index;
            }
        }

        // Set stations
        for (int i = 0; i < nr_stations; i++) {
            _x[i] = x[station_number[i]];
            _y[i] = y[station_number[i]];
            _z[i] = z[station_number[i]];
        }

        // Swap pointers and free memory
        double *__x = x;
        double *__y = y;
        double *__z = z;
        x = _x;
        y = _y;
        z = _z;
        free(__x);
        free(__y);
        free(__z);
    }

    // Define observation parameters
    double ra0  = RIGHT_ASCENSION;
    double dec0 = DECLINATION;
    double start_time_mjd = uvwsim_datetime_to_mjd(YEAR, MONTH, DAY, HOUR, MINUTE, SECONDS);
    double obs_length_hours = (nr_time * integration_time) / (3600.0);
    double obs_length_days = obs_length_hours / 24.0;

    // Allocate memory for baseline coordinates
    int nr_coordinates = nr_time * nr_baselines;
    double *uu = (double*) malloc(nr_coordinates * sizeof(double));
    double *vv = (double*) malloc(nr_coordinates * sizeof(double));
    double *ww = (double*) malloc(nr_coordinates * sizeof(double));

    // Evaluate baseline uvw coordinates.
    for (int t = 0; t < nr_time; t++) {
        double time_mjd = start_time_mjd + t
            * (obs_length_days/(double)nr_time);
        size_t offset = t * nr_baselines;
        uvwsim_evaluate_baseline_uvw(
            &uu[offset], &vv[offset], &ww[offset],
            nr_stations, x, y, z, ra0, dec0, time_mjd);
    }

    // Fill UVW datastructure
    for (int bl = 0; bl < nr_baselines; bl++) {
        for (int t = 0; t < nr_time; t++) {
            int i = t * nr_baselines + bl;
            UVW value = {(float) uu[i], (float) vv[i], (float) ww[i]};
            (*uvw)[bl][t] = value;
        }
    }

    // Free memory
    free(x); free(y); free(z);
    free(uu); free(vv); free(ww);
}
